class User < ActiveRecord::Base
    include Mongoid::Document
    include Mongoid::Timestamps
    # include Mongoid::Enum
    field :address, type: Hash, default: {}
    field :first_name, type: String
    field :last_name, type: String
    field :age, type: Integer, default: 1
    field :gender, type: Integer, default: 0


    validates_presence_of :first_name, :last_name
    validates_numericality_of :age, greater_than: 0, only_integer: true
	# validates :first_name, :last_name, presence: true
	# validates :age, numericality: { greater_than: 0 }

	enum gender: [ :male, :female, :others ]
end
